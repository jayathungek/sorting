def split(A):
	sublists = []
	s = len(A)/2
	i = 0
	j = 0
	while i<2:			
		sublist = []
		while j<s:
			sublist.append(A[j])
			j += 1
			
		sublists += [sublist]
		i += 1
		j = s
		s = len(A)
	return sublists

print split([1,2,3,4])